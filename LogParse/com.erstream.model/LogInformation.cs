﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LogParse.com.erstrem.model
{
    class LogInformation
    {
        private String userIpAddress;
        public String UserIpAddress
        {
            get { return userIpAddress; }
            set { userIpAddress = value; }
        }


        private String eventTime;
        public String EventTime
        {
            get { return eventTime; }
            set { eventTime = value; }
        }


        private String requestMethod;
        public String RequestMethod
        {
            get { return requestMethod; }
            set { requestMethod = value; }
        }


        private String contentAddress;
        public String ContentAddress
        {
            get { return contentAddress; }
            set { contentAddress = value; }
        }


        private String contentName;
        public String ContentName
        {
            get { return contentName; }
            set { contentName = value; }
        }


        private long contentBitRate;
        public long ContentBitRate
        {
            get { return contentBitRate; }
            set { contentBitRate = value; }
        }


        private String contentType;
        public String ContentType
        {
            get { return contentType; }
            set { contentType = value; }
        }


        private int httpStatus;
        public int HttpStatus
        {
            get { return httpStatus; }
            set { httpStatus = value; }
        }


        private int totalSentBytes;
        public int TotalSentBytes
        {
            get { return totalSentBytes; }
            set { totalSentBytes = value; }
        }


        private String userAgent;
        public String UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }


        private String cacheStatus;
        public String CacheStatus
        {
            get { return cacheStatus; }
            set { cacheStatus = value; }

        }

    }
}
