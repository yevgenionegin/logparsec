﻿using LogParse.com.erstream.data;
using LogParse.com.erstrem.model;
using LogParse.com.erstrem.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogParse.com.erstream.manager
{
    class AppManager
    {
        private String filePath;
        private LogInformation logInformation = new LogInformation();
        private TextParser textParser = new TextParser();
        private LogData logDatas = new LogData();

        public AppManager(String filePath)
        {

            this.filePath = filePath;

        }

        public void start()
        {

            try
            {
                StreamReader reader = new StreamReader(filePath);
                String line;

                while ((line = reader.ReadLine()) != null)
                {

                    try
                    {

                        logInformation = textParser.parse(line);

                        if (!(logInformation == null))
                        {
                            logDatas.put(logInformation);
                        }
                        

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        continue;
                    }
                }

                logDatas.setStatisticData();
            }
            catch (Exception)
            {
                throw;
            }



        }
    }
}