﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogParse.com.erstream.data
{
    class StatisticData
    {
        private static StatisticData statisticData;

        private StatisticData()
        {

        }

        public static StatisticData getObject()
        {

            if (statisticData == null)
            {
                statisticData = new StatisticData();
            }
            return statisticData;

        }


        private int uniqueIpCount;
        public int UniqueIpCount
        {
            get { return uniqueIpCount; }
            set { uniqueIpCount = value; }
        }


        private String theMostWiewingUser;
        public String TheMostWiewingUser
        {
            get { return theMostWiewingUser; }
            set { theMostWiewingUser = value; }
        }


        private int uniqueContentCount;
        public int UniqueContentCount
        {
            get { return uniqueContentCount; }
            set { uniqueContentCount = value; }
        }


        private String theMostWiewedContent;
        public String TheMostWiewedContent
        {
            get { return theMostWiewedContent; }
            set { theMostWiewedContent = value; }
        }


        private String theMostEnteredBrowser;
        public String TheMostEnteredBrowser
        {
            get { return theMostEnteredBrowser; }
            set { theMostEnteredBrowser = value; }
        }


        private String theMostViewedBitRate;
        public String TheMostViewedBitRate
        {
            get { return theMostViewedBitRate; }
            set { theMostViewedBitRate = value; }
        }


        private long totalSentGigabyte;
        public long TotalSentGigabyte
        {
            get { return totalSentGigabyte; }
            set { totalSentGigabyte = value; }
        }


        private String httpStatus;
        public String HttpStatus
        {
            get { return httpStatus; }
            set { httpStatus = value; }
        }
    }
}
