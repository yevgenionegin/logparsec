﻿using LogParse.com.erstrem.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LogParse.com.erstrem.parser
{
    class TextParser
    {

        private LogInformation logInformation = new LogInformation();

        private String[] parseWords;
        private String[] parseContent;
        private int leftEventTime;
        private int rigtEventTime;
        private int leftContentBitrate;
        private int rightContentBitrate;
        private int leftContentName;
        private int rigtContentName;
        private int leftContentType;
        private int rightContentType;
        private int leftCacheStatus;

        
        public LogInformation parse(String line)
        {

            try
            {
                parseWords = line.Replace("\"", "").Split(' ');

                leftEventTime = line.IndexOf("[")+1;
                rigtEventTime = line.IndexOf("]");
                leftContentName = parseWords[6].IndexOf("/ID_") +1;
                rigtContentName = parseWords[6].IndexOf(".ism/");
                leftCacheStatus = parseWords[15].IndexOf("=");

                logInformation.UserIpAddress = parseWords[0];
                logInformation.EventTime = line.Substring(leftEventTime, rigtEventTime-leftEventTime);
                logInformation.RequestMethod = parseWords[5];
                logInformation.ContentAddress = parseWords[6];
                logInformation.ContentName = parseWords[6].Substring(leftContentName, rigtContentName-leftContentName);

                parseContent = parseWords[6].Split('/');
                leftContentBitrate = parseContent[4].IndexOf("(")+1;
                rightContentBitrate = parseContent[4].IndexOf(")");
                leftContentType = parseContent[5].IndexOf("(")+1;
                rightContentType = parseContent[5].IndexOf("=");

                logInformation.ContentBitRate = Convert.ToInt64(parseContent[4].Substring(leftContentBitrate, rightContentBitrate-leftContentBitrate));
                logInformation.ContentType = parseContent[5].Substring(leftContentType, rightContentType-leftContentType);
                logInformation.HttpStatus = Convert.ToInt32(parseWords[8]);
                logInformation.TotalSentBytes = Convert.ToInt32(parseWords[9]);
                logInformation.UserAgent = parseWords[11];
                logInformation.CacheStatus = parseWords[15].Substring(leftCacheStatus + 1);

            }
            catch (Exception ex)
            {

                return null;
                
            }

            return logInformation;
        }

    }
}
