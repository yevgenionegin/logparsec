﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogParse.com.erstream.data;

namespace LogParse
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            StatisticData data = StatisticData.getObject();

            listView1.View = View.Details;

            listView1.Columns.Add("uniqueIpCount", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("theMostWiewingUser", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("uniqueContentCount", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("theMostWiewedContent", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("theMostEnteredBrowser", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("theMostViewedBitRate", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("totalSentGigabyte", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("httpStatus", 100, HorizontalAlignment.Left);

            int size = listView1.Items.Count;

            listView1.Items.Add(data.UniqueIpCount+"");
            listView1.Items[size].SubItems.Add(data.TheMostWiewingUser);
            listView1.Items[size].SubItems.Add(data.UniqueContentCount+"");
            listView1.Items[size].SubItems.Add(data.TheMostWiewedContent);
            listView1.Items[size].SubItems.Add(data.TheMostEnteredBrowser);
            listView1.Items[size].SubItems.Add(data.TheMostViewedBitRate);
            listView1.Items[size].SubItems.Add(data.TotalSentGigabyte+"");
            listView1.Items[size].SubItems.Add(data.HttpStatus);

        }
    }
}
