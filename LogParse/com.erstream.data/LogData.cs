﻿using LogParse.com.erstrem.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogParse.com.erstream.data
{
    class LogData
    {

        StatisticData statisticData = StatisticData.getObject();

        private Dictionary<String, int> ipIDatas = new Dictionary<String, int>();
        private Dictionary<String, int> contentNames = new Dictionary<String, int>();
        private Dictionary<String, int> userAgents = new Dictionary<String, int>();
        private Dictionary<String, int> contentbitrates = new Dictionary<String, int>();
        private Dictionary<String, int> httpStatus = new Dictionary<String, int>();

        private long totalSentBytes = 0;
        private int defaultvalue = 1;
        private int theBiggestValue = 0;
        private String mostViwed = "";
        private int GIGABYTE = 1024 * 1024 * 1024;
        private int DENOMİNATOR = 100000;

        private String userIpAdress;
        private String contentName;
        private String userAgent;
        private String contentBitrate;
        private String status;



        public void put(LogInformation data)
        {

            userIpAdress = data.UserIpAddress;
            contentName = data.ContentName;
            userAgent = data.UserAgent;
            contentBitrate = bitRateParse(data.ContentBitRate).ToString();
            status = data.HttpStatus.ToString();

            if (ipIDatas.ContainsKey(userIpAdress))
            {
                //ipIDatas.Add(userIpAdress, ipIDatas[userIpAdress] + 1);
                ipIDatas[userIpAdress] += 1;

            }
            else
            {
                ipIDatas.Add(userIpAdress, defaultvalue);
            }

            if (contentNames.ContainsKey(contentName))
            {
                //contentNames.Add(contentName, contentNames[contentName] + 1);
                contentNames[contentName] += 1;

            }
            else
            {
                contentNames.Add(contentName, defaultvalue);
            }

            if (userAgents.ContainsKey(userAgent))
            {
                //userAgents.Add(userAgent, userAgents[userAgent] + 1);
                userAgents[userAgent] += 1;

            }
            else
            {
                userAgents.Add(userAgent, defaultvalue);
            }

            if (contentbitrates.ContainsKey(contentBitrate))
            {
                //contentbitrates.Add(contentBitrate, contentbitrates[contentBitrate] + 1);
                contentbitrates[contentBitrate] += 1;
            }
            else
            {
                contentbitrates.Add(contentBitrate, defaultvalue);
            }

            if (httpStatus.ContainsKey(status))
            {
                //httpStatus.Add(status, httpStatus[status] + 1);
                httpStatus[status] += 1;
            }
            else
            {
                httpStatus.Add(status, defaultvalue);
            }

            totalSentBytes = totalSentBytes + data.TotalSentBytes;

        }

        public void setStatisticData()
        {

            statisticData.UniqueIpCount = ipIDatas.Count;
            statisticData.TheMostWiewingUser = getKeyMaxValue(ipIDatas);
            statisticData.UniqueContentCount = contentNames.Count;
            statisticData.TheMostWiewedContent = getKeyMaxValue(contentNames);
            statisticData.TheMostEnteredBrowser = getRatio(userAgents);
            statisticData.TheMostViewedBitRate = getRatio(contentbitrates);
            statisticData.HttpStatus = getRatio(httpStatus);
            statisticData.TotalSentGigabyte = (totalSentBytes / GIGABYTE);

        }


        public String getKeyMaxValue(Dictionary<String, int> map)
        {

            int value;

            foreach (KeyValuePair<String, int> entry in map)
            {

                value = entry.Value;

                if (value > theBiggestValue)
                {

                    theBiggestValue = value;
                    mostViwed = entry.Key;

                }
            }

            return mostViwed;

        }

        public String getRatio(Dictionary<String, int> map)
        {

            StringBuilder builder = new StringBuilder();

            double totalValue = 0;
            double resultValue = 0;
            double keyValue = 0;

            ArrayList keyList = new ArrayList();

            foreach (KeyValuePair<String, int> entry in map)
            {

                totalValue = totalValue + entry.Value;

                keyList.Add(entry.Key);

            }


            foreach (String key in keyList)
            {
                keyValue = Convert.ToDouble(map[key]);
                resultValue = (keyValue / totalValue * 100);
                builder.Append(key + "-" + resultValue.ToString("#.##"));
                builder.Append(" ");

            }

            return builder.ToString();

        }

        public long bitRateParse(long value)
        {

            int size = (int)(Math.Log10(value) + 1);

            if (size >= 6)
            {
                return ((value / DENOMİNATOR) * DENOMİNATOR);
            }
            else
            {
                switch (size)
                {
                    case 5:
                        value = ((value / 10000) * 1000);
                        break;
                    case 4:
                        value = ((value / 1000) * 100);
                        break;
                    case 3:
                        value = ((value / 100) * 1000);
                        break;
                    case 2:
                        value = ((value / 10) * 10);
                        break;

                    default:
                        break;
                }
            }

            return value;
        }

    }
}
